#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Exercise number 1."""

import os
import random
import numpy as np

from mcs1.ensemble import BaggingBasic
from mcs1.datasets import read_dataset
from sklearn.metrics import accuracy_score
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MinMaxScaler
from sklearn.tree import DecisionTreeClassifier

import matplotlib
matplotlib.use("Svg")
import matplotlib.pyplot as plt

class BaggingKNN(BaggingBasic):
    """Represents the bagging ensemble of KNNs."""

    def __init__(self, n_neighbors=1, n_classifiers=1):
        """Creates a new instance of the classifier."""
        super(BaggingKNN, self).__init__(n_classifiers)
        for i in range(n_classifiers):
            self.classifiers[i] = KNeighborsClassifier(n_neighbors=n_neighbors)

class NaiveBayesBagging(BaggingBasic):
    """Represents the bagging ensemble of Naive Bayes."""

    def __init__(self, n_classifiers=1):
        """Creates a new instance of the classifier."""
        super(NaiveBayesBagging, self).__init__(n_classifiers)
        for i in range(n_classifiers):
            self.classifiers[i] = GaussianNB()

class DecisionTreeBagging(BaggingBasic):
    """Represents the bagging ensemble of DecisionTrees."""

    def __init__(self, n_classifiers=1, max_depth=5):
        """Creates a new instance of the classifier."""
        super(DecisionTreeBagging, self).__init__(n_classifiers)
        for i in range(n_classifiers):
            self.classifiers[i] = DecisionTreeClassifier(max_depth=max_depth,
                                                         random_state=2345)

def evaluate_nb(pool_sizes, objects, labels, figures_dir, dataset_name):
    """Evaluates the NaiveBayes pool."""
    accuracies = list()
    for pool_size in pool_sizes:
        splits = StratifiedShuffleSplit(n_splits=10, random_state=2000)
        scores = list()
        for train_ind, test_ind in splits.split(objects, labels):
            X_train, X_test = objects[train_ind], objects[test_ind]
            y_train, y_test = labels[train_ind], labels[test_ind]
            nb = NaiveBayesBagging(n_classifiers=pool_size)
            nb.fit(X_train, y_train)
            y_pred = nb.predict(X_test)
            scores.append(accuracy_score(y_test, y_pred))
        accuracies.append(np.mean(scores))
    plt.figure()
    plt.plot(pool_sizes, accuracies, 'k', lw=2)
    plt.xticks(pool_sizes)
    plt.title("{0} Dataset - Naive Bayes Pool (Bagging)".format(dataset_name))
    plt.xlabel("Tamanho do Pool")
    plt.ylabel("Taxa de Acerto Media (Validacao Cruzada 10-fold)")
    plt.savefig(os.path.join(figures_dir,
                             "{0}_bagging_nb.svg".format(dataset_name)),
                format="svg")
    return accuracies

def evaluate_knn(pool_sizes, objects, labels, figures_dir, dataset_name):
    """Evaluates the KNN pool."""
    accuracies = list()
    for pool_size in pool_sizes:
        splits = StratifiedShuffleSplit(n_splits=10, random_state=2000)
        scores = list()
        for train_ind, test_ind in splits.split(objects, labels):
            X_train, X_test = objects[train_ind], objects[test_ind]
            y_train, y_test = labels[train_ind], labels[test_ind]
            # cross validation to tune the hyper parameter
            val_splits = StratifiedShuffleSplit(n_splits=10, random_state=3456)
            val_scores = dict()
            for k in [1,3,5,7,11]:
                k_scores = list()
                for train2_ind, val_ind in val_splits.split(X_train, y_train):
                    X_train2, X_val = X_train[train2_ind], X_train[val_ind]
                    y_train2, y_val = y_train[train2_ind], y_train[val_ind]
                    knn = BaggingKNN(n_neighbors=k, n_classifiers=pool_size)
                    knn.fit(X_train2, y_train2)
                    y_pred = knn.predict(X_val)
                    k_scores.append(accuracy_score(y_val, y_pred))
                val_scores[k] = np.mean(k_scores)
            best_k = max(val_scores, key=lambda key: val_scores[key])
            knn = BaggingKNN(n_neighbors=best_k, n_classifiers=pool_size)
            knn.fit(X_train, y_train)
            y_pred = knn.predict(X_test)
            scores.append(accuracy_score(y_test, y_pred))
        accuracies.append(np.mean(scores))
    plt.figure()
    plt.plot(pool_sizes, accuracies, 'k', lw=2)
    plt.xticks(pool_sizes)
    plt.title("{0} Dataset - KNN Pool (Bagging)".format(dataset_name))
    plt.xlabel("Tamanho do Pool")
    plt.ylabel("Taxa de Acerto Media (Validacao Cruzada 10-fold)")
    plt.savefig(os.path.join(figures_dir,
                             "{0}_bagging_knn.svg".format(dataset_name)),
                format="svg")
    return accuracies

def evaluate_dt(pool_sizes, objects, labels, figures_dir, dataset_name):
    """Evaluates the decision tree pool."""
    accuracies = list()
    for pool_size in pool_sizes:
        splits = StratifiedShuffleSplit(n_splits=10, random_state=2000)
        scores = list()
        for train_ind, test_ind in splits.split(objects, labels):
            X_train, X_test = objects[train_ind], objects[test_ind]
            y_train, y_test = labels[train_ind], labels[test_ind]
            # cross validation to tune the hyper parameter
            val_splits = StratifiedShuffleSplit(n_splits=10, random_state=3456)
            val_scores = dict()
            for max_depth in [5, 10, 15, 20]:
                max_depth_scores = list()
                for train2_ind, val_ind in val_splits.split(X_train, y_train):
                    X_train2, X_val = X_train[train2_ind], X_train[val_ind]
                    y_train2, y_val = y_train[train2_ind], y_train[val_ind]
                    dectree = DecisionTreeBagging(n_classifiers=pool_size,
                                              max_depth=max_depth)
                    dectree.fit(X_train2, y_train2)
                    y_pred = dectree.predict(X_val)
                    max_depth_scores.append(accuracy_score(y_val, y_pred))
                val_scores[max_depth] = np.mean(max_depth_scores)
            best_depth = max(val_scores, key=lambda key: val_scores[key])
            dectree = DecisionTreeBagging(n_classifiers=pool_size,
                                          max_depth=best_depth)
            dectree.fit(X_train, y_train)
            y_pred = dectree.predict(X_test)
            scores.append(accuracy_score(y_test, y_pred))
        accuracies.append(np.mean(scores))
    plt.figure()
    plt.plot(pool_sizes, accuracies, 'k', lw=2)
    plt.xticks(pool_sizes)
    plt.title("{0} Dataset - Decision Tree Pool (Bagging)".format(dataset_name))
    plt.xlabel("Tamanho do Pool")
    plt.ylabel("Taxa de Acerto Media (Validacao Cruzada 10-fold)")
    plt.savefig(os.path.join(figures_dir,
                             "{0}_bagging_dt.svg".format(dataset_name)),
                format="svg")
    return accuracies

def main():
    """Main function."""
    random.seed(2000)
    figures_dir = "figures"
    if not os.path.exists(figures_dir):
        os.makedirs(figures_dir)

    seeds_objects, seeds_labels = read_dataset("resources/seeds_dataset.txt",
                                               separator="\t",
                                               has_missing=False)
    trans_objects, trans_labels = read_dataset("resources/transfusion.data",
                                               separator=",",
                                               has_header=True)
    segme_objects, segme_labels = read_dataset("resources/segmentation.data",
                                               separator=",",
                                               has_header=True,
                                               class_column=0)

    pool_sizes = [1] + list(range(10, 110, 10)) # 1, 10, 20, 30, ..., 100

    # Scale features
    seeds_objects = MinMaxScaler().fit_transform(seeds_objects)
    trans_objects = MinMaxScaler().fit_transform(trans_objects)
    segme_objects = MinMaxScaler().fit_transform(segme_objects)

    nb_acc_seeds = evaluate_nb(pool_sizes, seeds_objects, seeds_labels,
                               figures_dir, "Seeds")
    nb_acc_trans = evaluate_nb(pool_sizes, trans_objects, trans_labels,
                               figures_dir, "Transfusion")
    nb_acc_segme = evaluate_nb(pool_sizes, segme_objects, segme_labels,
                               figures_dir, "Segmentation")
    knn_acc_seeds = evaluate_knn(pool_sizes, seeds_objects, seeds_labels,
                                 figures_dir, "Seeds")
    knn_acc_trans = evaluate_knn(pool_sizes, trans_objects, trans_labels,
                                 figures_dir, "Transfusion")
    knn_acc_segme = evaluate_knn(pool_sizes, segme_objects, segme_labels,
                                 figures_dir, "Segmentation")
    dt_acc_seeds = evaluate_dt(pool_sizes, seeds_objects, seeds_labels,
                               figures_dir, "Seeds")
    dt_acc_trans = evaluate_dt(pool_sizes, trans_objects, trans_labels,
                               figures_dir, "Transfusion")
    dt_acc_segme = evaluate_dt(pool_sizes, segme_objects, segme_labels,
                               figures_dir, "Segmentation")
    print("Base Seeds:")
    print("NaiveBayes = {0}".format(np.mean(nb_acc_seeds)))
    print("KNN = {0}".format(np.mean(knn_acc_seeds)))
    print("DecisionTree = {0}".format(np.mean(dt_acc_seeds)))
    print("")
    print("Base Transfusion:")
    print("NaiveBayes = {0}".format(np.mean(nb_acc_trans)))
    print("KNN = {0}".format(np.mean(knn_acc_trans)))
    print("DecisionTree = {0}".format(np.mean(dt_acc_trans)))
    print("")
    print("Base Segmentation")
    print("NaiveBayes = {0}".format(np.mean(nb_acc_segme)))
    print("KNN = {0}".format(np.mean(knn_acc_segme)))
    print("DecisionTree = {0}".format(np.mean(dt_acc_segme)))

if __name__ == "__main__":
    main()
