#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests the ensemble implementations."""

import math
import random
import unittest
import numpy as np

from ..ensemble import bootstrap_sample, MajorVotingEnsemble, \
                       select_random_subspace, RandomSubspaceBasic

class TestBootstrapSample(unittest.TestCase):
    """Tests the boostrap_sample function"""

    def setUp(self):
        random.seed(0)
        self.elems = [
            [0, 6, 7],
            [1, 8, 9],
            [2, 10, 11],
            [3, 12, 13],
            [4, 14, 15],
            [5, 16, 17]
        ]
        self.classes = [1, 1, 1, 2, 2, 2]
        self.objects = np.array(self.elems)
        self.labels = np.array(self.classes)

    def test_same_size(self):
        """Tests the function when new_size is not specified"""
        objects2, labels2 = bootstrap_sample(self.objects, self.labels)
        self.assertEqual(objects2.shape[0], 6)
        self.assertEqual(objects2.shape[1], 3)
        self.assertEqual(labels2.shape[0], 6)
        elems2 = []
        for obj in objects2:
            lobj = list(obj)
            elems2.append(lobj)
            self.assertTrue(lobj in self.elems)
        not_in = 0
        for elem in self.elems:
            if elem not in elems2:
                not_in += 1
        self.assertTrue(not_in > 0)
        # check if the labels have been properly assigned
        for i in range(objects2.shape[0]):
            self.assertEqual(labels2[i], self.classes[objects2[i, 0]])

    def test_new_size(self):
        """Tests the function when new_size is specified"""
        objects2, labels2 = bootstrap_sample(
            self.objects, self.labels, new_size=4)
        self.assertEqual(objects2.shape[0], 4)
        self.assertEqual(objects2.shape[1], 3)
        self.assertEqual(labels2.shape[0], 4)

class MockClassifier(object):
    """Dummy classifier."""
    instance_counter = 0

    def __init__(self):
        self.instance_id = int(math.ceil(MockClassifier.instance_counter / 2.0))
        MockClassifier.instance_counter += 1

    def predict(self, X):
        """Dummy predict method."""
        return np.array([self.instance_id for _ in range(X.shape[0])])

class TestMajorVotingEnsemble(unittest.TestCase):
    """Tests the major voting ensembler implementation."""

    def test_predict(self):
        """Tests the predict method for a Major Voting ensembled classifier."""
        clfier = MajorVotingEnsemble(n_classifiers=4, classifier=MockClassifier)
        # the mock classifier is setup so the first classifier always predicts 0
        # the second and third classifiers always predict 1 and the fourth
        # always predict 2. That way, the ensemble prediction should be 1
        y = clfier.predict(np.array([[1, 2]]))
        self.assertEqual(y.shape[0], 1)
        self.assertEqual(y[0], 1)

class TestSelectRandomSubspace(unittest.TestCase):
    """Tests the Random Subspace selection function."""

    def test_select_random_subspace(self):
        """Tests the function."""
        new = select_random_subspace([], percentage=0.5)
        self.assertEqual([], new)
        new = select_random_subspace(range(3), percentage=0.0)
        self.assertEqual([], new)
        new = select_random_subspace(range(4), percentage=0.5)
        self.assertEqual(2, len(new))
        self.assertNotEqual(new[0], new[1])
        new = select_random_subspace(range(3), percentage=0.5)
        self.assertEqual(1, len(new))
        new = select_random_subspace(range(10), percentage=0.2)
        self.assertEqual(2, len(new))
        new = select_random_subspace(range(10), percentage=0.3)
        self.assertEqual(3, len(new))
        i = 1
        while i < 3:
            self.assertNotEqual(new[i], new[i - 1])
            i += 1
        new = select_random_subspace(range(10), percentage=0.5)
        self.assertEqual(5, len(new))
        i = 1
        while i < 5:
            self.assertNotEqual(new[i], new[i - 1])
            i += 1

class MockSpaceClassifier(object):
    """A mock classifier for Random Subspace unit test."""

    def __init__(self):
        self.columns = list()

    def fit(self, X, y):
        """The fit method."""
        for i in range(X.shape[1]):
            self.columns.append(X[0, i])

class TestRandomSubspaceBasic(unittest.TestCase):
    """Test the Random Subspace basic implementation."""

    def test_fit(self):
        """Tests the fit method."""
        ensemble = RandomSubspaceBasic(n_classifiers=10,
                                       classifier=MockSpaceClassifier,
                                       features_percentage=0.2)
        ensemble.fit(
            np.array([
                [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                [10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
                [20, 21, 22, 23, 24, 25, 26, 27, 28, 29],
                [30, 31, 32, 33, 34, 35, 36, 37, 38, 39],
            ]),
            np.array([
                1, 2, 3, 4
            ])
        )
        different = 0
        for classifier in ensemble.classifiers:
            self.assertEqual(2, len(classifier.columns))
            i = 0
            for column in classifier.columns:
                self.assertTrue(column in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
                if i > 0:
                    self.assertNotEqual(classifier.columns[i],
                                        classifier.columns[i - 1])
                i += 1
            for classifier2 in ensemble.classifiers:
                if classifier.columns != classifier2.columns:
                    different += 1
        self.assertTrue(different > 0)

if __name__ == "__main__":
    unittest.main()
