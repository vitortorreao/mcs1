#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Exercise number 2."""

import os
import random
import numpy as np

from mcs1.ensemble import RandomSubspaceBasic
from mcs1.datasets import read_dataset, trigonometric_feature_expansion
from sklearn.metrics import accuracy_score
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MinMaxScaler
from sklearn.tree import DecisionTreeClassifier

import matplotlib
matplotlib.use("Svg")
import matplotlib.pyplot as plt

class KNNRandomSubspace(RandomSubspaceBasic):
    """The KNN random subspace ensemble classifier."""

    def __init__(self, n_classifiers=1, percentage=0.5, n_neighbors=1):
        """Creates a new instance of the classifier."""
        super(KNNRandomSubspace, self).__init__(n_classifiers,
                                                features_percentage=percentage)
        for i in range(n_classifiers):
            self.classifiers[i] = KNeighborsClassifier(n_neighbors=n_neighbors)

class DecisionTreeRandomSubspace(RandomSubspaceBasic):
    """The decision tree random subspace ensemble classifier."""

    def __init__(self, n_classifiers=1, percentage=0.5, max_depth=5):
        """Creates a new instance of the ensemble classifier."""
        super(DecisionTreeRandomSubspace, self).__init__(
            n_classifiers,
            features_percentage=percentage)
        for i in range(n_classifiers):
            self.classifiers[i] = DecisionTreeClassifier(max_depth=max_depth,
                                                         random_state=5432)

def evaluate_knn(pool_sizes, objects, labels, figures_dir, dataset_name,
                 percentage):
    """Evaluates the KNN pool."""
    accuracies = list()
    for pool_size in pool_sizes:
        splits = StratifiedShuffleSplit(n_splits=10, random_state=2000)
        scores = list()
        for train_ind, test_ind in splits.split(objects, labels):
            X_train, X_test = objects[train_ind], objects[test_ind]
            y_train, y_test = labels[train_ind], labels[test_ind]
            # cross validation to tune the hyper parameter
            val_splits = StratifiedShuffleSplit(n_splits=10, random_state=3456)
            val_scores = dict()
            for k in [1, 3, 5, 7, 11]:
                k_scores = list()
                for train2_ind, val_ind in val_splits.split(X_train, y_train):
                    X_train2, X_val = X_train[train2_ind], X_train[val_ind]
                    y_train2, y_val = y_train[train2_ind], y_train[val_ind]
                    knn = KNNRandomSubspace(n_neighbors=k,
                                            n_classifiers=pool_size,
                                            percentage=percentage)
                    knn.fit(X_train2, y_train2)
                    y_pred = knn.predict(X_val)
                    k_scores.append(accuracy_score(y_val, y_pred))
                val_scores[k] = np.mean(k_scores)
            best_k = max(val_scores, key=lambda key: val_scores[key])
            knn = KNNRandomSubspace(n_neighbors=best_k, n_classifiers=pool_size,
                                    percentage=percentage)
            knn.fit(X_train, y_train)
            y_pred = knn.predict(X_test)
            scores.append(accuracy_score(y_test, y_pred))
        accuracies.append(np.mean(scores))
    plt.figure()
    plt.plot(pool_sizes, accuracies, lw=2)
    plt.xticks(pool_sizes)
    plt.title("{0} Dataset - KNN Pool (Random Subset)".format(dataset_name))
    plt.xlabel("Tamanho do Pool")
    plt.ylabel("Taxa de Acerto Media (Validacao Cruzada 10-fold)")
    plt.savefig(os.path.join(figures_dir,
                             "{0}_randsub_{1}_knn.svg".format(
                                 dataset_name,
                                 str(percentage).replace(".", ""))),
                format="svg")
    return accuracies

def evaluate_dt(pool_sizes, objects, labels, figures_dir, dataset_name,
                percentage):
    """Evaluates the decision tree pool."""
    accuracies = list()
    for pool_size in pool_sizes:
        splits = StratifiedShuffleSplit(n_splits=10, random_state=2000)
        scores = list()
        for train_ind, test_ind in splits.split(objects, labels):
            X_train, X_test = objects[train_ind], objects[test_ind]
            y_train, y_test = labels[train_ind], labels[test_ind]
            # cross validation to tune the hyper parameter
            val_splits = StratifiedShuffleSplit(n_splits=10, random_state=3456)
            val_scores = dict()
            for max_depth in [5, 10, 15, 20]:
                max_depth_scores = list()
                for train2_ind, val_ind in val_splits.split(X_train, y_train):
                    X_train2, X_val = X_train[train2_ind], X_train[val_ind]
                    y_train2, y_val = y_train[train2_ind], y_train[val_ind]
                    dectree = DecisionTreeRandomSubspace(n_classifiers=pool_size,
                                                         max_depth=max_depth,
                                                         percentage=percentage)
                    dectree.fit(X_train2, y_train2)
                    y_pred = dectree.predict(X_val)
                    max_depth_scores.append(accuracy_score(y_val, y_pred))
                val_scores[max_depth] = np.mean(max_depth_scores)
            best_depth = max(val_scores, key=lambda key: val_scores[key])
            dectree = DecisionTreeRandomSubspace(n_classifiers=pool_size,
                                                 max_depth=best_depth,
                                                 percentage=percentage)
            dectree.fit(X_train, y_train)
            y_pred = dectree.predict(X_test)
            scores.append(accuracy_score(y_test, y_pred))
        accuracies.append(np.mean(scores))
    plt.figure()
    plt.plot(pool_sizes, accuracies, 'k', lw=2)
    plt.xticks(pool_sizes)
    plt.title("{0} Dataset - Decision Tree Pool (Random Subset)".format(
        dataset_name))
    plt.xlabel("Tamanho do Pool")
    plt.ylabel("Taxa de Acerto Media (Validacao Cruzada 10-fold)")
    plt.savefig(os.path.join(figures_dir,
                             "{0}_randsub_{1}_dt.svg".format(
                                 dataset_name,
                                 str(percentage).replace(".", ""))),
                format="svg")
    return accuracies

def main():
    """Main function."""
    random.seed(2000)
    figures_dir = "figures"
    if not os.path.exists(figures_dir):
        os.makedirs(figures_dir)

    seeds_objects, seeds_labels = read_dataset("resources/seeds_dataset.txt",
                                               separator="\t",
                                               has_missing=False)
    trans_objects, trans_labels = read_dataset("resources/transfusion.data",
                                               separator=",",
                                               has_header=True)
    pool_sizes = [1] + list(range(10, 110, 10)) # 1, 10, 20, 30, ..., 100
    # Expand the features:
    seeds_objects = trigonometric_feature_expansion(seeds_objects)
    trans_objects = trigonometric_feature_expansion(trans_objects)
    # Scale features:
    seeds_objects = MinMaxScaler().fit_transform(seeds_objects)
    trans_objects = MinMaxScaler().fit_transform(trans_objects)

    for percentage in [0.3, 0.4, 0.5]:
        seeds_accuracies = evaluate_dt(pool_sizes, seeds_objects, seeds_labels,
                                       figures_dir, "Seeds", percentage)
        trans_accuracies = evaluate_dt(pool_sizes, trans_objects, trans_labels,
                                       figures_dir, "Transfusion", percentage)
        print("CART Ensemble (with " + str(percentage) + " Random Subspace) " +
              "in the Seeds dataset had average accuracy = " +
              str(np.mean(seeds_accuracies)))
        print("CART Ensemble (with " + str(percentage) + " Random Subspace) " +
              "in the Transfusion dataset had average accuracy = " +
              str(np.mean(trans_accuracies)))

if __name__ == "__main__":
    main()
